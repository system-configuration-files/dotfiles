;; Telegram
(require 'telega)
;; enable emoji completion
(add-hook 'telega-chat-mode-hook
          (lambda ()
            (set (make-local-variable 'company-backends)
                 (append '(telega-company-emoji
                           telega-company-username
                           telega-company-hashtag)
                         (when (telega-chat-bot-p telega-chatbuf--chat)
                           '(telega-company-botcmd))))
            (company-mode 1)))
;; emoji support
(add-hook 'after-init-hook #'global-emojify-mode)
;; enable notifications
(telega-notifications-mode 1)
