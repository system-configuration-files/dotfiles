;; leader keybinds
;; Evil Nerd Comments
(evil-leader/set-key
  "bm" 'buffer-menu
  "cc" 'evilnc-copy-and-comment-lines
  "ci" 'evilnc-comment-or-uncomment-lines
  "cj" 'cider-jack-in-clj
  "cl" 'evilnc-quick-comment-or-uncomment-to-the-line
  "cp" 'evilnc-comment-or-uncomment-paragraphs
  "cr" 'comment-or-uncomment-region
  "cs" 'cider-jack-in-cljs
  "cu" '(lambda ()
          (interactive)
          (goto-char (mark)))
  "cv" 'evilnc-toggle-invert-comment-line-by-line
  "gt" 'centaur-tabs-forward-group
  "gT" 'centaur-tabs-backward-group
  "jd" '(lambda ()
          (interactive)
          (evil-goto-definition))
  "kb" '(lambda ()
          (interactive)
          (kill-buffer))
  "kt" '(lambda ()
          (interactive)
          (elscreen-kill-screen-and-buffers))
  "kw" '(lambda ()
          (interactive)
          (kill-buffer-and-window))
  "ll" 'evilnc-quick-comment-or-uncomment-to-the-linhorizontally
  "rl" '(lambda ()
          (interactive)
          (load-file "~/.emacs.d/init.el"))
  "sh" 'split-below-shell
  "ss" '(lambda ()
          (interactive)
          (vterm))
  "te" '(lambda ()
          (interactive)
          (telega))
  "th" '(lambda ()
          (interactive)
          (vterm-term-horizontal))
  "tt" 'treemacs
  "tv" '(lambda ()
          (interactive)
          (vterm-term-vertical)
          (centaur-tabs-local-mode))
  "."  'evilnc-copy-and-comment-operator
  "\\" 'evilnc-comment-operator
  "P"    'evil-visual-replace-query-replace
  "p"  'evil-visual-replace-replace-regexp
)

;; Evil split management
;; Opening splits
(global-set-key (kbd "C-x |") 'evil-window-vsplit)
(global-set-key (kbd "C-x -") 'evil-window-split)

;; Move between splits in an Evil manner
(global-set-key (kbd "C-x h") 'windmove-left)
(global-set-key (kbd "C-x j") 'windmove-down)
(global-set-key (kbd "C-x k") 'windmove-up)
(global-set-key (kbd "C-x l") 'windmove-right)

;; Evil escape
;; (evil-escape-mode)
;; (setq-default evil-escape-delay 0.2)
;; (global-set-key (kbd "C-c j") 'evil-escape)
;; (setq-default evil-escape-key-sequence "fdd")

;; Overload shifts so that they don't lose the selection
(define-key evil-visual-state-map (kbd ">") 'djoyner/evil-shift-right-visual)
(define-key evil-visual-state-map (kbd "<") 'djoyner/evil-shift-left-visual)
(define-key evil-visual-state-map [tab] 'djoyner/evil-shift-right-visual)
(define-key evil-visual-state-map [S-tab] 'djoyner/evil-shift-left-visual)

(defun djoyner/evil-shift-left-visual ()
  (interactive)
  (evil-shift-left (region-beginning) (region-end))
  (evil-normal-state)
  (evil-visual-restore))

(defun djoyner/evil-shift-right-visual ()
  (interactive)
  (evil-shift-right (region-beginning) (region-end))
  (evil-normal-state)
  (evil-visual-restore))

(require 'treemacs-evil)
(require 'evil-visual-replace)

(defcustom evil-kill-on-visual-paste t
  "Whether ~evil-visual-paste' adds the replaced text to the kill
ring, making it the default for the next paste. The default, t,
replicates the default vim behavior."
  :type 'boolean
  :group 'evil)

(setq evil-kill-on-visual-paste nil)

(fset 'evil-visual-update-x-selection 'ignore)
