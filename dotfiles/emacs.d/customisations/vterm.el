;; Split Shell
(defun split-below-shell ()
  (interactive)
  (let ((w (split-window-below)))
    (select-window w)
    (shrink-window 11)
    (vterm)
    (centaur-tabs-local-mode)))

(defun run-vertical (func args)
  "Take a FUNC, with string of ARGS and run it in a vertical split."
  (interactive "aCommand to run in split:\nsArgs: ")
  (let ((w (split-window-below)))
    (select-window w)
    (funcall func args)))

(defun run-horizontal (func args)
  "Take a FUNC, with string of ARGS and run it in a horizontal split."
  (interactive "aCommand to run in split:\nsArgs: ")
  (let ((w (split-window-right)))
    (select-window w)
    (funcall func args)))

(defun vterm-term-horizontal ()
  (interactive)
  (let ((w (split-window-right)))
    (select-window w)
    (vterm)))

(defun vterm-term-vertical ()
  (interactive)
  (let ((w (split-window-below)))
    (select-window w)
    (vterm)))

;; Vterm
(setq-default vterm-kill-buffer-on-exit t)
(add-to-list 'evil-emacs-state-modes 'vterm-mode)

;; rebind paste in term-mode to term-paste
(evil-define-key 'normal vterm-mode-map "p" 'vterm-yank)
(evil-define-key 'normal vterm-mode-map "P" '(lambda ()
                                               (interactive)
                                               (vterm-send-left)
                                               (vterm-yank)))

