;; Groovy
(use-package lsp-mode
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  :init (setq lsp-keymap-prefix "s-l")
  :hook ((groovy-mode . lsp)
         (lsp-mode .))
  :commands lsp)

