;;; Package -- summary
;;; Commentary:
;;;;
;; Wikoion's vimrc,  I mean Evil Emacs init.el
;;;;

;;;;
;; Packages
;;;;
;; Define package repositories
(require 'package)
(add-to-list 'package-archives
             '("tromey" . "http://tromey.com/elpa/") t)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)

(add-to-list 'package-pinned-packages '(cider . "melpa-stable") t)

(add-to-list 'package-pinned-packages '(cider . "melpa-stable") t)

;; Load and activate emacs packages. Do this first so that the
;; packages are loaded before you start trying to modify them.
;; This also sets the load path.
(package-initialize)

;; Download the ELPA archive description if needed.
;; This informs Emacs about the latest versions of all packages, and
;; makes them available for download.
(when (not package-archive-contents)
  (package-refresh-contents))
    
;; Define he following variables to remove the compile-log warnings
;; when defining ido-ubiquitous
;; (defvar ido-cur-item nil)
;; (defvar ido-default-item nil)
;; (defvar ido-cur-list nil)
;; (defvar predicate nil)
;; (defvar inherit-input-method nil)

;; The packages you want installed. You can also install these
;; manually with M-x package-install
(defvar my-packages
  '(;; makes handling lisp expressions much, much easier
    ;; Cheatsheet: http://www.emacswiki.org/emacs/PareditCheatsheet
    paredit
    use-package
    ;; allow ido usage in as many contexts as possible. see
    ;; customisations/navigation.el line 23 for a description
    ;; of ido
    ido-completing-read+
    ;; Enhances M-x to allow easier execution of commands. Provides
    ;; a filterable list of possible commands in the minibuffer
    ;; http://www.emacswiki.org/emacs/Smex
    smex
    ;; project navigation
    projectile
    ;; colorful parenthesis matching
    rainbow-delimiters
    ;; edit html tags like sexps
    tagedit
    ;; git integration
    magit
    ;; search
    deadgrep
    ripgrep

    emojify
    unicode-fonts
    centaur-tabs
    ;; Evil (vim bindings)
    evil
    evil-collection
    ;; Quote/un-quote words
    evil-surround
    ;; multi-line comments
    evil-nerd-commenter
    ;; vim leader key
    evil-leader
    ;; replace
    evil-visual-replace
    ;; undo
    undo-fu
    simple-httpd
    oauth2
    protobuf-mode
    company
    lsp-ui
    dap-mode
    treemacs
    treemacs-evil
    yasnippet
    hydra
    helm
    flycheck
    xclip
    doom-themes
    highlight-indent-guides
    smartparens
    evil-smartparens

    ;;;;
    ;; LANGUAGE MODES
    ;;;; 

    ;; Rust
    rust-mode
    rustic
    racer
    company-racer

    ;; Clojure
    clojure-mode
    clojure-mode-extra-font-locking
    cider

    ;; Terraform
    terraform-mode
    terraform-doc
    company-terraform

    ;; Docker
    dockerfile-mode
    docker-compose-mode
    kubernetes-evil

    ;; Golang
    go-mode
    go-complete

    ;; Java
    groovy-mode
    javap-mode
    lsp-java
    kotlin-mode

    ;; Python
    python-mode

    ;; Ruby
    enh-ruby-mode
    company-inf-ruby
    rubocop
    ))

;; On OS X, an Emacs instance started from the graphical user
;; interface will have a different environment than a shell in a
;; terminal window, because OS X does not run a shell during the
;; login. Obviously this will lead to unexpected results when
;; calling external utilities like make from Emacs.
;; This library works around this problem by copying important
;; environment variables from the user's shell.
;; https://github.com/purcell/exec-path-from-shell
(if (eq system-type 'darwin)
    (add-to-list 'my-packages 'exec-path-from-shell))

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))


;; Place downloaded elisp files in ~/.emacs.d/vendor. You'll then be able
;; to load them.
;;
;; For example, if you download yaml-mode.el to ~/.emacs.d/vendor,
;; then you can add the following code to this file:
;;
;; (require 'yaml-mode)
;; (add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
;; 
;; Adding this code will make Emacs enter yaml mode whenever you open
;; a .yml file
(add-to-list 'load-path "~/.emacs.d/vendor")
(add-to-list 'load-path "~/.emacs.d/vendor/spotify.el")

;;;;
;; Customisation
;;;;

;; Package imports
;; Enable Evil mode
(use-package evil
  :ensure t
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

;; Evil surround
(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))

(global-evil-leader-mode)
(evil-leader/set-leader ",")

;; Completion
(require 'lsp)
(global-company-mode)
(dap-mode t)
(dap-ui-mode t)

;; Smartparens
(require 'smartparens-config)
(add-hook 'smartparens-enabled-hook #'evil-smartparens-mode)

;; GPG Setup
(require 'epa-file)
(setq epa-pinentry-mode 'loopback)
(setq epg-gpg-program "/usr/local/bin/gpg")
(setq auth-sources
    '((:source "~/.emacs.d/secrets/.authinfo.gpg")))

(require 'protobuf-mode)
(require 'midnight)

;; Unicode font support
  (use-package unicode-fonts
   :ensure t
   :config 
  (unicode-fonts-setup))

;; Telega
;; (use-package telega
;;   :load-path  "~/.emacs.d/vendor/telega.el"
;;   :commands (telega)
;;   :defer t)

;; Add a directory to our load path so that when you `load` things
;; below, Emacs knows where to look for the corresponding file.
(add-to-list 'load-path "~/.emacs.d/customisations")

;; Sets up exec-path-from-shell so that Emacs will use the correct
;; environment variables
(load "shell-integration.el")

;; Setup EVIL mode for vim like navigation
(load "evil.el")

;; These customisations make it easier for you to navigate files,
;; switch buffers, and choose options from the minibuffer.
(load "navigation.el")

;; These customisations change the way emacs looks and disable/enable
;; some user interface elements
(load "ui.el")

;; These customisations make editing a bit nicer.
(load "editing.el")
;; Hard-to-categorize customisations
(load "misc.el")

;; Copy to clipboard
(load "os-specific.el")

;; For editing lisps
(load "elisp-editing.el")

;; Langauage-specific
(load "clojure.el")
(load "js.el")
(load "ruby.el")
(load "rust.el")
(load "go.el")
(load "kotlin.el")
;; (load "groovy.el")
(load "java.el")

;; Vterm setup
(load "vterm.el")

;; Misc app setup
(load "setup-slack.el")
;; (load "telegram.el")

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(coffee-tab-width 2)
 '(custom-safe-themes
   '("99ea831ca79a916f1bd789de366b639d09811501e8c092c85b2cb7d697777f93" default))
 '(helm-minibuffer-history-key "M-p")
 '(package-selected-packages
   '(ripgrep deadgrep csv-mode csv evil-visual-replace undo-fu jsonnet-mode treemacs-projectile company-terraform rustic slack telega racer rust-mode protobuf-mode highlight-indent-guides evil-collection vterm term-run emojify kubernetes-evil docker-compose-mode terraform-mode lua-mode yaml-mode evil-surround go-complete treemacs-evil treemacs-persp neotree dirtree atom-dark-theme enh-ruby-mode autopair yasnippet groovy-mode company-inf-ruby rubocop company magit tagedit rainbow-delimiters projectile smex ido-completing-read+ cider clojure-mode-extra-font-locking clojure-mode paredit exec-path-from-shell))
 '(safe-local-variable-values
   '((cider-shadow-default-options . "app")
     (cider-default-cljs-repl . shadow)
     (cider-offer-to-open-cljs-app-in-browser)
     (cider-offer-to-open-app-in-browser))))
(epa-file-enable)

(provide 'init)
;;; init.el ends here
