#!/usr/bin/env bb

(require '[clojure.java.shell :refer [sh]])

(def user (System/getenv "USER"))
(def vfio-path (str "/home/" user "/.VFIOinput/"))

(defn vfio-cmd [cmd vm path]
  (:exit (sh "virsh" cmd vm path)))

(defn plug-devices [cmd vm & devices]
  (let [paths (map
               (fn [device] (str vfio-path "input_" device ".xml"))
               devices)]
    (for [path paths]
     (vfio-cmd cmd vm path))))

(let [output (plug-devices "attach-device" "win10" 1 2)]
  (if (contains? (set output) 0)
    (println "Successfully attached")
    (->
     (plug-devices "detach-device" "win10" 1 2)
     (println "Successfully detached"))))
