#!/bin/bash

monitors=$(xrandr -q | grep " connected" | cut -d ' ' -f1)

h=$(pgrep -f polybar)

while `killall polybar`; do `killall polybar`; done
sleep 1
while `killall polybar`; do `killall polybar`; done
sleep 1

i=1
for mon in "${monitors}"; do
    mon=$(echo $monitors | cut -d ' ' -f$i)
    MONITOR="$mon" polybar -q i3-bar-$i 2>/dev/null
    i=$((i+1))
done

for mon in "${monitors}"; do
    mon=$(echo $monitors | cut -d ' ' -f2)
    MONITOR="$mon" polybar -q i3-bar-$i 2>/dev/null
    i=$((i+1))
done
echo $i

for mon in "${monitors}"; do
    mon=$(echo $monitors | cut -d ' ' -f3)
    MONITOR="$mon" polybar -q i3-bar-$i 2>/dev/null
    i=$((i+1))
done
