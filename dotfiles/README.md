# Dotfiles
I use yadm to manage these hence them being nested inside another folder in the repo.
### Dependencies
Install the following dependencies:

- yadm
- zsh
- oh-my-zsh
- tmux
- vim or nvim
- starship (rust based multishell prompt)
- polybar
- i3-gaps
- rofi


for arch:
```
yay -S yadm zsh oh-my-zsh tmux vim starship polybar i3-gaps rofi
```

### Installation
```
yadm clone git@gitlab.com:system-configuration-files/dotfiles.git
cd dotfiles
./install.sh
```

