#!/bin/bash 
suspendfile=~/dotfiles/suspend/suspend.txt
declare -a programs

readarray -t programs < ~/dotfiles/suspend/suspend.txt

for program in "${programs[@]}"; do
    if "$program" -eq "idea"; then
        `idea-ce-eap`
    elif "$program" -eq "i3lock"; then
        continue
    else `"$program"`
    fi
    echo "" > $suspendfile
done
