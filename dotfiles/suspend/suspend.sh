#!/bin/bash 
programs=("idea" "signal-desktop" "i3lock")
suspendpath=~/dotfiles/suspend/suspend.txt
declare -a killedprograms

if [ "${1}" == "pre" ]; then
    if [ ! -f $suspendpath ]; then
     touch $suspendpath
    fi

    for program in "${programs[@]}"; do 
        pid=$(ps cax | grep $program)

        if [ "$(pgrep $program)" ]; then
        killedprograms+=("$program")
        pkill -f $program
        fi
    done

elif [ "${1}" == "post" ]; then
    for program in "${killedprograms[@]}"; do
        if "$program" -eq "idea"; then
            `idea-ce-eap`
        elif "$program" -eq "i3lock"; then
            continue
        else `"$program"`
        fi
    done
fi

