#!/bin/bash
############################
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
############################

###########################
# Variables
###########################

dir=~/dotfiles                    # dotfiles directory
olddir=~/dotfiles_old             # old dotfiles backup directory
files=(~/dotfiles/*)
installer=~/dotfiles/install.sh
readme=~/dotfiles/README.md
config=~/dotfiles/config
configfiles=(~/dotfiles/config/*)
lsp=~/dotfiles/config/lsp
kde=~/dotfiles/kde
kdefiles=(~/dotfiles/kde/*)
i3=~/dotfiles/i3
i3files=(~/dotfiles/i3/*)
obsscripts=(~/dotfiles/obs-scripts)

excluded=($installer $readme $config $kde $i3 $obsscripts)

############################
# Functions
############################

backupFile() {
    echo "Moving any existing dotfiles from ~ to $olddir"
    mv -n ~/.${1##*/} ~/dotfiles_old/
}

symlinkFile() {
    echo "Creating symlink to $1 in ~ directory."
    ln -nsf $1 ~/.${1##*/}
}

backupConfigFile() {
    echo "Moving any existing dotfiles from ~/.config/${1##*/} to $olddir"
    mv -n ~/.config/${1##*/} ~/dotfiles_old/.config/
}

symlinkConfigFile() {
    echo "Creating symlink to $1 in ~/.config directory."
    ln -nsf $1 ~/.config/
}

backupAndSymlinkCoreFiles() {
    for file in "${files[@]}"; do
        for file in "${excluded[@]}"; do
          continue
        done

        backupFile $file
        symlinkFile $file
    done
}

backupAndSymlinkConfigFiles() {
    for file in "${configfiles[@]}"; do
        backupFile $file
        symlinkFile $file
    done
}

backupAndSymlinkKDEFiles() {
    if pacman -Qi plasma-desktop; then
        for file in "${kdefiles[@]}"; do
            backupConfigFile $file
            symlinkConfigFile $file
        done
    fi
}

backupAndSymlinkI3Files() {
    if pacman -Qi i3-gaps; then
        for file in "${i3files[@]}"; do
            backupConfigFile $file
            symlinkConfigFile $file
        done
    fi
}

# backup existing dotfiles and symlink new ones from dotfiles dir
backupFilesAndCreateSymlinks() {
    for file in "${files[@]}"; do
      # To be removed in favour of exluded list, needs testing
        if [ $file == $installer ] || [ $file == $readme ] || [ $file == $config ]; then
            continue
        fi

        for file in "${excluded[@]}"; do
          continue
        done

        echo "Moving any existing dotfiles from ~ to $olddir"
        mv -n ~/.${file##*/} ~/dotfiles_old/
        echo "Creating symlink to $file in home directory."
        ln -nsf $file ~/.${file##*/}
    done

    for file in "${configfiles[@]}"; do
        echo "Moving any existing dotfiles from ~/.config/${file##*/} to $olddir"
        mv -n ~/.config/${file##*/} ~/dotfiles_old/.config/
        echo "Creating symlink to $file in config directory."
        ln -nsf $file ~/.config/
    done
}

installLanguageServers() {
    # Install clojure-lsp
    cd "$lsp" && git clone https://github.com/snoe/clojure-lsp.git && chmod 755 .

    # Install kotlin-lsp
    cd "$lsp" && git clone https://github.com/fwcd/kotlin-language-server.git
    cd kotlin-language-server && ./gradlew :server:installDist

    # Install groovy-language-server
    cd "$lsp" && git clone https://github.com/prominic/groovy-language-server.git
    cd groovy-language-server && ./gradlew build
    # symlink to emacs dir (required for lsp-mode)
    ln -s "$dotfiles"/config/lsp/groovy-language-server/build/libs/groovy-language-server.jar emacs.d/groovy-language-server/groovy-language-server-all.jar

    # Source to update path
    source ~/.zshrc
}

updateLanguageServers() {
    # Update clojure-lsp
    cd "$lsp"/clojure-lsp && git fetch && git pull && chmod 755 .

    # Update kotlin-lsp
    cd "$lsp"/kotlin-language-server && git fetch && git pull && ./gradlew :server:installDist

    # Update groovy-lsp
    cd "$lsp"/groovy-language-server && git fetch && git pull && ./gradlew build
}

############################
# Logic
############################

#Set up command line flags and assign variables
# create dotfiles_old in homedir
echo -n "Creating $olddir for backup of any existing dotfiles in ~ ..."
mkdir -p $olddir
mkdir =p $olddir/.config
echo "done"

if [ $# -lt 1 ]; then
    # backupFilesAndCreateSymlinks
    backupAndSymlinkCoreFiles
    backupAndSymlinkKDEFiles
    backupAndSymlinkI3Files
    backupAndSymlinkConfigFiles
    
    installLanguageServers

    # update font cache
    echo "updating font cache"
    fc-cache -fv
fi

while (( "$#"  )); do

    case "$1" in

        -h|--help)
            echo "Run without parameters to do a clean install"
            echo "Run with --update flag to update all dotfiles and vim language servers"
            echo "Run with --update-lsp to update just the language servers"
            exit 0
        ;;

        --update)
            # create dotfiles_old in homedir
            echo -n "Creating $olddir for backup of any existing dotfiles in ~ ..."
            mkdir -p $olddir
            echo "done"

            # backupFilesAndCreateSymlinks
            backupAndSymlinkCoreFiles
            backupAndSymlinkKDEFiles
            backupAndSymlinkI3Files
            backupAndSymlinkConfigFiles
            updateLanguageServers

            # update font cache
            echo "updating font cache"
            fc-cache -fv

        ;;

         --update-lsp)
            updateLanguageServers
        ;;

   esac

   break

done
