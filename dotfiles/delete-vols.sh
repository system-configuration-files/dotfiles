#!/bin/bash
for subvolume in /var/lib/docker/btrfs/subvolumes/; do
    btrfs subvolume delete $subvolume
done
rm -rf /var/lib/docker
