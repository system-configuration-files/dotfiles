15:02:52.154: CPU Name: Intel(R) Core(TM) i7-10875H CPU @ 2.30GHz
15:02:52.154: CPU Speed: 4385.286MHz
15:02:52.155: Physical Cores: 8, Logical Cores: 16
15:02:52.155: Physical Memory: 15865MB Total, 168MB Free
15:02:52.155: Kernel Version: Linux 5.10.27-1-lts
15:02:52.155: Distribution: "EndeavourOS" Unknown
15:02:52.155: Session Type: x11
15:02:52.155: Window System: X11.0, Vendor: The X.Org Foundation, Version: 1.20.10
15:02:52.156: Portable mode: false
15:02:52.194: OBS 26.1.2-2 (linux)
15:02:52.195: ---------------------------------
15:02:52.195: ---------------------------------
15:02:52.195: audio settings reset:
15:02:52.195: 	samples per sec: 48000
15:02:52.195: 	speakers:        2
15:02:52.198: ---------------------------------
15:02:52.198: Initializing OpenGL...
15:02:52.282: Loading up OpenGL on adapter Intel Mesa Intel(R) UHD Graphics (CML GT2)
15:02:52.282: OpenGL loaded successfully, version 4.6 (Core Profile) Mesa 21.0.1, shading language 4.60
15:02:52.311: ---------------------------------
15:02:52.311: video settings reset:
15:02:52.311: 	base resolution:   1920x1080
15:02:52.311: 	output resolution: 1920x1080
15:02:52.311: 	downscale filter:  Bicubic
15:02:52.311: 	fps:               60/1
15:02:52.311: 	format:            NV12
15:02:52.311: 	YUV mode:          709/Partial
15:02:52.311: NV12 texture support not available
15:02:52.316: Audio monitoring device:
15:02:52.316: 	name: Default
15:02:52.316: 	id: default
15:02:52.316: ---------------------------------
15:02:52.320: Failed to load 'en-US' text for module: 'decklink-captions.so'
15:02:52.326: Failed to load 'en-US' text for module: 'decklink-ouput-ui.so'
15:02:52.436: A DeckLink iterator could not be created.  The DeckLink drivers may not be installed
15:02:52.436: No blackmagic support
15:02:52.455: v4l2loopback not installed, virtual camera disabled
15:02:52.498: NVENC supported
15:02:52.498: FFMPEG VAAPI supported
15:02:52.523: VLC found, VLC video source enabled
15:02:52.523: ---------------------------------
15:02:52.523:   Loaded Modules:
15:02:52.523:     vlc-video.so
15:02:52.523:     text-freetype2.so
15:02:52.523:     rtmp-services.so
15:02:52.523:     obs-x264.so
15:02:52.523:     obs-transitions.so
15:02:52.523:     obs-outputs.so
15:02:52.523:     obs-libfdk.so
15:02:52.523:     obs-filters.so
15:02:52.523:     obs-ffmpeg.so
15:02:52.523:     linux-v4l2.so
15:02:52.523:     linux-pulseaudio.so
15:02:52.523:     linux-jack.so
15:02:52.523:     linux-decklink.so
15:02:52.523:     linux-capture.so
15:02:52.523:     linux-alsa.so
15:02:52.523:     image-source.so
15:02:52.523:     frontend-tools.so
15:02:52.523:     decklink-ouput-ui.so
15:02:52.523:     decklink-captions.so
15:02:52.523: ---------------------------------
15:02:52.523: ==== Startup complete ===============================================
15:02:52.528: All scene data cleared
15:02:52.528: ------------------------------------------------
15:02:52.538: pulse-input: Server name: 'PulseAudio (on PipeWire 0.3.25) 14.0.0'
15:02:52.538: pulse-input: Audio format: s32le, 48000 Hz, 2 channels
15:02:52.538: pulse-input: Started recording from 'alsa_output.usb-Focusrite_Scarlett_2i2_USB_Y80B3JD9924184-00.analog-stereo.monitor'
15:02:52.538: [Loaded global audio device]: 'Desktop Audio'
15:02:52.539: pulse-input: Server name: 'PulseAudio (on PipeWire 0.3.25) 14.0.0'
15:02:52.539: pulse-input: Audio format: s32le, 48000 Hz, 2 channels
15:02:52.539: pulse-input: Started recording from 'alsa_input.pci-0000_00_1f.3.analog-stereo'
15:02:52.539: [Loaded global audio device]: 'Mic/Aux'
15:02:52.543: pulse-input: Server name: 'PulseAudio (on PipeWire 0.3.25) 14.0.0'
15:02:52.543: pulse-input: Audio format: s32le, 48000 Hz, 2 channels
15:02:52.543: pulse-input: Started recording from 'alsa_input.usb-Focusrite_Scarlett_2i2_USB_Y80B3JD9924184-00.analog-stereo'
15:02:52.547: pulse-input: Server name: 'PulseAudio (on PipeWire 0.3.25) 14.0.0'
15:02:52.548: pulse-input: Audio format: s32le, 48000 Hz, 2 channels
15:02:52.548: pulse-input: Started recording from 'alsa_input.usb-Focusrite_Scarlett_2i2_USB_Y80B3JD9924184-00.analog-stereo'
15:02:52.548: [Media Source 'Waiting Music']: settings:
15:02:52.548: 	input:                   /home/ender/dotfiles/config/obs-studio/assets/waiting-music.mp3
15:02:52.548: 	input_format:            (null)
15:02:52.548: 	speed:                   100
15:02:52.548: 	is_looping:              no
15:02:52.548: 	is_hw_decoding:          no
15:02:52.548: 	is_clear_on_media_end:   yes
15:02:52.548: 	restart_on_activate:     yes
15:02:52.548: 	close_when_inactive:     no
15:02:52.548: [Media Source 'Countdown music']: settings:
15:02:52.548: 	input:                   /home/ender/dotfiles/config/obs-studio/assets/countdown-music.mp3
15:02:52.548: 	input_format:            (null)
15:02:52.548: 	speed:                   100
15:02:52.548: 	is_looping:              no
15:02:52.548: 	is_hw_decoding:          no
15:02:52.548: 	is_clear_on_media_end:   yes
15:02:52.548: 	restart_on_activate:     yes
15:02:52.548: 	close_when_inactive:     no
15:02:52.864: xshm-input: Geometry 2560x1080 @ 1920,0
15:02:52.874: v4l2-input: Start capture from /dev/video5
15:02:52.876: v4l2-input: Unable to set input 0
15:02:52.876: v4l2-input: Initialization failed
15:02:52.956: pulse-input: Server name: 'PulseAudio (on PipeWire 0.3.25) 14.0.0'
15:02:52.956: pulse-input: Audio format: s16le, 48000 Hz, 2 channels
15:02:52.956: pulse-input: Started recording from 'alsa_output.usb-DisplayLink_Dell_Universal_Dock_D6000_1806254478-02.analog-stereo.monitor'
15:02:52.957: xshm-input: Geometry 2560x1080 @ 1920,0
15:02:52.961: pulse-input: Server name: 'PulseAudio (on PipeWire 0.3.25) 14.0.0'
15:02:52.961: pulse-input: Audio format: s32le, 48000 Hz, 2 channels
15:02:52.962: pulse-input: Started recording from 'alsa_output.usb-Focusrite_Scarlett_2i2_USB_Y80B3JD9924184-00.analog-stereo.monitor'
15:02:52.962: v4l2-input: Start capture from /dev/video4
15:02:52.962: v4l2-input: Input: 0
15:02:52.991: v4l2-input: Resolution: 1920x1080
15:02:52.991: v4l2-input: Pixelformat: YUYV
15:02:52.991: v4l2-input: Linesize: 3840 Bytes
15:02:52.991: v4l2-input: Framerate: 24.00 fps
15:02:53.001: Switched to scene 'Presentation'
15:02:53.001: ------------------------------------------------
15:02:53.001: Loaded scenes:
15:02:53.001: - scene 'Paused':
15:02:53.001:     - source: 'VLC Video Source' (vlc_source)
15:02:53.001:     - source: 'Main Overlay' (image_source)
15:02:53.001:     - source: 'BRB' (image_source)
15:02:53.001:     - source: 'Back Soon' (text_ft2_source_v2)
15:02:53.001:     - source: 'Break-Timer' (text_ft2_source_v2)
15:02:53.001:     - source: 'Waiting Music' (ffmpeg_source)
15:02:53.001: - scene 'Starting Soon':
15:02:53.001:     - source: 'VLC Video Source' (vlc_source)
15:02:53.001:     - source: 'Main Overlay' (image_source)
15:02:53.001:     - source: 'BRB' (image_source)
15:02:53.001:     - source: 'Countdown' (text_ft2_source_v2)
15:02:53.001:     - source: 'Starting Soon Text' (text_ft2_source_v2)
15:02:53.001:     - source: 'Countdown music' (ffmpeg_source)
15:02:53.001: - scene 'Gaming':
15:02:53.001:     - source: 'Screen Capture (XSHM)' (xshm_input)
15:02:53.001:     - source: 'Video Capture Device (V4L2)' (v4l2_input)
15:02:53.001:     - source: 'Main Overlay' (image_source)
15:02:53.001:     - source: 'Image 2' (image_source)
15:02:53.001:     - source: 'Audio Output Capture (PulseAudio)' (pulse_output_capture)
15:02:53.001:         - filter: 'Compressor' (compressor_filter)
15:02:53.001:     - source: 'Audio Input Capture (PulseAudio)' (pulse_input_capture)
15:02:53.001:         - filter: 'Compressor' (compressor_filter)
15:02:53.001:         - filter: 'Noise Suppression' (noise_suppress_filter)
15:02:53.001: - scene 'Screencap':
15:02:53.001:     - source: 'Screen Capture (XSHM) 2' (xshm_input)
15:02:53.002:     - source: 'Audio Output Capture (PulseAudio)' (pulse_output_capture)
15:02:53.002:         - filter: 'Compressor' (compressor_filter)
15:02:53.002:     - source: 'Window Capture (Xcomposite)' (xcomposite_input)
15:02:53.002: - scene 'Presentation':
15:02:53.002:     - source: 'Video Capture Device (V4L2) 2' (v4l2_input)
15:02:53.002:     - source: 'Audio Input Capture (PulseAudio) 2' (pulse_input_capture)
15:02:53.002: - scene 'Scene 2':
15:02:53.002:     - source: 'Audio Output Capture (PulseAudio) 2' (pulse_output_capture)
15:02:53.002: - scene 'Scene 3':
15:02:53.002: ------------------------------------------------
15:02:53.109: adding 21 milliseconds of audio buffering, total audio buffering is now 21 milliseconds (source: Desktop Audio)
15:02:53.109: 
15:04:21.371: adding 21 milliseconds of audio buffering, total audio buffering is now 42 milliseconds (source: Audio Output Capture (PulseAudio) 2)
15:04:21.371: 
15:04:24.615: adding 192 milliseconds of audio buffering, total audio buffering is now 234 milliseconds (source: Audio Input Capture (PulseAudio))
15:04:24.615: 
15:04:36.397: adding 64 milliseconds of audio buffering, total audio buffering is now 298 milliseconds (source: Audio Input Capture (PulseAudio))
15:04:36.397: 
15:04:37.648: adding 192 milliseconds of audio buffering, total audio buffering is now 490 milliseconds (source: Audio Input Capture (PulseAudio))
15:04:37.648: 
15:14:04.975: v4l2-input: Stopped capture after 16342 frames
15:14:04.976: v4l2-helpers: unable to stop stream
15:16:18.559: v4l2-input: Found device 'Cam Link 4K: Cam Link 4K' at /dev/video5
15:16:18.559: v4l2-input: /dev/video3 seems to not support video capture
15:16:18.560: v4l2-input: /dev/video1 seems to not support video capture
15:16:18.560: v4l2-input: /dev/video6 seems to not support video capture
15:16:18.705: v4l2-input: Found device 'Integrated Camera: Integrated I' at /dev/video2
15:16:18.707: v4l2-input: Found device 'Integrated Camera: Integrated C' at /dev/video0
15:16:18.718: v4l2-input: Start capture from /dev/video4
15:16:18.718: v4l2-input: Unable to open device
15:16:18.718: v4l2-input: Initialization failed
15:16:20.847: v4l2-input: Start capture from /dev/video5
15:16:20.999: v4l2-input: Found input 'Input 1' (Index 0)
15:16:20.999: v4l2-input: Input: 0
15:16:21.030: v4l2-input: Resolution: 1920x1080
15:16:21.030: v4l2-input: Pixelformat: YUYV
15:16:21.030: v4l2-input: Linesize: 3840 Bytes
15:16:21.030: v4l2-input: Framerate: 59.94 fps
15:16:21.032: v4l2-input: Pixelformat: YUYV 4:2:2 (available)
15:16:21.032: v4l2-input: Pixelformat: Y/CbCr 4:2:0 (available)
15:16:21.032: v4l2-input: Pixelformat: Planar YUV 4:2:0 (available)
15:16:21.032: v4l2-input: Pixelformat: RGB3 (Emulated) (unavailable)
15:16:21.032: v4l2-input: Pixelformat: BGR3 (Emulated) (available)
15:16:21.032: v4l2-input: Pixelformat: YV12 (Emulated) (available)
15:16:21.033: v4l2-input: Stepwise and Continuous framerates are currently hardcoded
15:17:58.334: ---------------------------------
15:17:58.334: [x264 encoder: 'simple_h264_recording'] preset: veryfast
15:17:58.334: [x264 encoder: 'simple_h264_recording'] profile: high
15:17:58.337: [x264 encoder: 'simple_h264_recording'] settings:
15:17:58.337: 	rate_control: CRF
15:17:58.337: 	bitrate:      0
15:17:58.337: 	buffer size:  0
15:17:58.337: 	crf:          16
15:17:58.337: 	fps_num:      60
15:17:58.337: 	fps_den:      1
15:17:58.337: 	width:        1920
15:17:58.337: 	height:       1080
15:17:58.337: 	keyint:       250
15:17:58.337: 
15:17:58.638: libfdk_aac encoder created
15:17:58.638: libfdk_aac bitrate: 192, channels: 2
15:17:58.663: ==== Recording Start ===============================================
15:17:58.663: [ffmpeg muxer: 'simple_file_output'] Writing file '/home/ender/Videos/2021-04-10_15-17-58.mkv'...
15:18:18.462: adding 192 milliseconds of audio buffering, total audio buffering is now 682 milliseconds (source: Audio Input Capture (PulseAudio))
15:18:18.462: 
15:21:02.218: [ffmpeg muxer: 'simple_file_output'] Output of file '/home/ender/Videos/2021-04-10_15-17-58.mkv' stopped
15:21:02.218: Output 'simple_file_output': stopping
15:21:02.218: Output 'simple_file_output': Total frames output: 10947
15:21:02.218: Output 'simple_file_output': Total drawn frames: 8954 (11014 attempted)
15:21:02.218: Output 'simple_file_output': Number of lagged frames due to rendering lag/stalls: 2060 (18.7%)
15:21:02.218: ==== Recording Stop ================================================
15:21:02.220: Video stopped, number of skipped frames due to encoding lag: 31/11006 (0.3%)
15:21:02.317: libfdk_aac encoder destroyed
15:25:05.963: ---------------------------------
15:25:05.963: [x264 encoder: 'simple_h264_recording'] preset: veryfast
15:25:05.963: [x264 encoder: 'simple_h264_recording'] profile: high
15:25:05.963: [x264 encoder: 'simple_h264_recording'] settings:
15:25:05.963: 	rate_control: CRF
15:25:05.963: 	bitrate:      0
15:25:05.963: 	buffer size:  0
15:25:05.963: 	crf:          16
15:25:05.963: 	fps_num:      60
15:25:05.963: 	fps_den:      1
15:25:05.963: 	width:        1920
15:25:05.963: 	height:       1080
15:25:05.963: 	keyint:       250
15:25:05.963: 
15:25:06.043: libfdk_aac encoder created
15:25:06.043: libfdk_aac bitrate: 192, channels: 2
15:25:06.045: ==== Recording Start ===============================================
15:25:06.045: [ffmpeg muxer: 'simple_file_output'] Writing file '/home/ender/Videos/2021-04-10_15-25-05.mkv'...
15:27:41.236: [ffmpeg muxer: 'simple_file_output'] Output of file '/home/ender/Videos/2021-04-10_15-25-05.mkv' stopped
15:27:41.236: Output 'simple_file_output': stopping
15:27:41.236: Output 'simple_file_output': Total frames output: 9251
15:27:41.236: Output 'simple_file_output': Total drawn frames: 8966 (9312 attempted)
15:27:41.236: Output 'simple_file_output': Number of lagged frames due to rendering lag/stalls: 346 (3.7%)
15:27:41.236: Video stopped, number of skipped frames due to encoding lag: 26/9302 (0.3%)
15:27:41.236: ==== Recording Stop ================================================
15:27:41.300: libfdk_aac encoder destroyed
15:36:03.634: ---------------------------------
15:36:03.635: [x264 encoder: 'simple_h264_recording'] preset: veryfast
15:36:03.635: [x264 encoder: 'simple_h264_recording'] profile: high
15:36:03.635: [x264 encoder: 'simple_h264_recording'] settings:
15:36:03.635: 	rate_control: CRF
15:36:03.635: 	bitrate:      0
15:36:03.635: 	buffer size:  0
15:36:03.635: 	crf:          16
15:36:03.635: 	fps_num:      60
15:36:03.635: 	fps_den:      1
15:36:03.635: 	width:        1920
15:36:03.635: 	height:       1080
15:36:03.635: 	keyint:       250
15:36:03.635: 
15:36:03.746: libfdk_aac encoder created
15:36:03.746: libfdk_aac bitrate: 192, channels: 2
15:36:03.751: ==== Recording Start ===============================================
15:36:03.751: [ffmpeg muxer: 'simple_file_output'] Writing file '/home/ender/Videos/2021-04-10_15-36-03.mkv'...
15:36:50.422: [ffmpeg muxer: 'simple_file_output'] Output of file '/home/ender/Videos/2021-04-10_15-36-03.mkv' stopped
15:36:50.422: Output 'simple_file_output': stopping
15:36:50.422: Output 'simple_file_output': Total frames output: 2747
15:36:50.422: Output 'simple_file_output': Total drawn frames: 2434 (2799 attempted)
15:36:50.422: Output 'simple_file_output': Number of lagged frames due to rendering lag/stalls: 365 (13.0%)
15:36:50.422: Video stopped, number of skipped frames due to encoding lag: 99/2795 (3.5%)
15:36:50.425: ==== Recording Stop ================================================
15:36:50.603: libfdk_aac encoder destroyed
