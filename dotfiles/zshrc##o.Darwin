export ZSH="${HOME}/.oh-my-zsh"

ZSH_THEME="amuse"

plugins=(git)

source $ZSH/oh-my-zsh.sh

alias ohmyzsh="mate ~/.oh-my-zsh"
alias k='kubectl'
alias kx='kubectx'
alias kn='kubens'
alias pscli="$HOME/bin/pscli"
alias vc='codium'
alias sz='source $HOME/.zshrc'
alias kd='kubectl describe'
alias kdp='kubectl describe pod'
alias delp='kubectl delete pod'
alias kl='kubectl logs -f'
alias ke='kubectl edit'

neofetch

export PATH="$HOME/bin:$PATH"
export PATH="/Users/Richard.Draycott/bin/pscli:$PATH"
export PATH=/usr/local/bin:/usr/local/sbin:$PATH 
export PATH="$HOME/Library/Python/3.9/bin:$PATH"

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export PATH="$HOME/bin/spring-2.1.5/bin:$PATH"

# Go
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin;
export PATH=$PATH:$GOPATH/bin/gopls;

# Kotlin
export PATH=$PATH:$HOME/dotfiles/config/lsp/kotlin-language-server/server/build/install/server/bin

# Groovy
export PATH=$PATH:$HOME/dotfiles/config/lsp/groovy-language-server/build/libs

# GNU Core utils
export PATH="/usr/local/opt/coreutils/libexec/gnubin:${PATH}"
export MANPATH="/usr/local/opt/coreutils/libexec/gnuman:${MANPATH}"
export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"

# Telega
export PATH=$PATH:$HOME/git/td/build/td
export PATH="$HOME/.cargo/bin:$PATH"

# Functions
# ----------
# Misc
function rgrep() {
    grep -r "$*" *
}

# Git
function gitm() {
    git add . && git commit -m "$*"
}

function gitmp() {
    branch=$(git rev-parse --abbrev-ref HEAD)
    git add . && git commit -m "$*" && git push -u origin $branch
}

function gitp() {
    branch=$(git rev-parse --abbrev-ref HEAD)
    git push -u origin $branch
}

function gitfp() {
    git fetch && git pull
}

# Yadm
function yadmm() {
    yadm add ~/dotfiles && yadm commit -m "$*"
}

function yadmmp() {
    yadm add ~/dotfiles && yadm commit -m "$*" && yadm push
}

function yadmfp() {
    yadm fetch && yadm pull
}

function yadmp() {
    yadm push
}

function tnam() {
    tmux rename-window -t $1 "$2"
}

# K8s
function kgp() {
    kubectl get pod $1 -o yaml
}

function kg() {
    kubectl get $1 $2 -o yaml
}

function kexit() {
    kubectl exec -it $* -- /bin/bash
}

function kex() {
    kubectl exec -it $1 -- $2
}

# Curl
function curlp() {
    endpoint="$1"
    arg="$endpoint"
    auth="-a"
    dataInput="$2"
    
    data="{"
    curl=( curl -i -X POST )
    curlAuth=( -H "Authorization: Bearer $TOKEN" )
    curlData=( -d )
    curlDataBin=( --data-binary )

    if [ -z "${arg##*$auth*}" ] ;then
       endpoint="$2"
       dataInput="$3"
       curl=( "$curl[@]" "$curlAuth[@]" "$endpoint" )
    fi

    curl=( "$curl[@]" "$endpoint")

    if [ -z "${dataInput##*$data*}" ] ;then
       curl=( "$curl[@]" "$curlData[@]")
    else 
       curl=( "$curl[@]" "$curlDataBin[@]")
    fi

    "$curl[@] $dataInput" 
}

function curlput() {
    endpoint="$1"
    arg="$endpoint"
    auth="-a"
    dataInput="$2"
    
    data="{"
    curl=( curl -i -X POST )
    curlAuth=( -H "Authorization: Bearer $TOKEN" )
    curlData=( -d )
    curlDataBin=( --data-binary )

    if [ -z "${arg##*$auth*}" ] ;then
       endpoint="$2"
       dataInput="$3"
       curl=( "$curl[@]" "$curlAuth[@]" "$endpoint" )
    fi

    curl=( "$curl[@]" "$endpoint")

    if [ -z "${dataInput##*$data*}" ] ;then
       curl=( "$curl[@]" "$curlData[@]")
    else 
       curl=( "$curl[@]" "$curlDataBin[@]")
    fi

    "$curl[@] $dataInput" 
}

function curlg() {
    endpoint="$1"
    arg="$endpoint"
    auth="-a"
    
    data="{"
    curl=( curl -i )
    curlAuth=( -H "Authorization: Bearer $TOKEN" )

    if [ -z "${arg##*$auth*}" ] ;then
       endpoint="$2"
       curl=( "$curl[@]" "$curlAuth[@]" "$endpoint" )
    fi

    curl=( "$curl[@]" "$endpoint")

    "$curl[@]" 
}

function curld() {
    endpoint="$1"
    arg="$endpoint"
    auth="-a"
    
    data="{"
    curl=( curl -i -X DELETE )
    curlAuth=( -H "Authorization: Bearer $TOKEN" )

    if [ -z "${arg##*$auth*}" ] ;then
       endpoint="$2"
       curl=( "$curl[@]" "$curlAuth[@]" "$endpoint" )
    fi

    curl=( "$curl[@]" "$endpoint")

    "$curl[@]" 
}

export PATH="/opt/homebrew/bin:$PATH"

# Set Spaceship ZSH as a prompt
eval "$(starship init zsh)"
#source ~/.kubech/kubech
#source ~/.kubech/completion/kubech.bash

# krew path
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
